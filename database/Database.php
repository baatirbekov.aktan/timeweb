<?php

namespace Database;

use App\Traits\SingletonTrait;
use PDO;

class Database
{
    use SingletonTrait;

    /**
     * @var PDO
     */
    private PDO $conn;

    private function __construct(
        private $host,
        private $user,
        private $password,
        private $db
    )
    {
        $dsn = "pgsql:host=$this->host;port=5432;dbname=$this->db;";
        $this->conn = new PDO($dsn, $this->user, $this->password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    /**
     * @return mixed
     */
    public static function getInstance(): mixed
    {
        if (!self::$instance) {
            $env = parse_ini_file('/var/www/.env');
            self::$instance = new Database(
                $env['DB_HOST'],
                $env['DB_USERNAME'],
                $env['DB_PASSWORD'],
                $env['DB_DATABASE']
            );
        }

        return self::$instance;
    }


    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->conn;
    }
}