<?php

namespace Database;

class QueryBuilder
{
    /**
     * @var string
     */
    private string $select = '';

    /**
     * @var string
     */
    private string $from = '';

    /**
     * @var string
     */
    private string $where = '';

    /**
     * @var string
     */
    private string $join = '';

    /**
     * @var string
     */
    private string $andWhere = '';

    /**
     * @param array $columns
     * @return $this
     */
    public function select(array $columns): static
    {
        if (!empty($columns)) {
            $this->select = 'SELECT ' . implode(', ', $columns);
        }
        return $this;
    }

    /**
     * @param string $table
     * @return $this
     */
    public function from(string $table): static
    {
        if (!empty($table)) {
            $this->from = 'FROM ' . $table;
        }
        return $this;
    }

    /**
     * @param string $condition
     * @return $this
     */
    public function where(string $condition): static
    {
        if (!empty($condition)) {
            if ($this->where == '') {
                $this->where = 'WHERE ' . $condition;
            } else {
                $this->andWhere .= 'AND ' . $condition;
            }
        }
        return $this;
    }

    /**
     * @param string $condition
     * @return $this
     */
    public function andWhere(string $condition): static
    {
        if (!empty($condition)) {
            $this->andWhere .= 'AND ' . $condition;
        }
        return $this;
    }

    /**
     * @param string $table
     * @param string $condition
     * @param string $type
     * @return $this
     */
    public function join(string $table, string $condition, string $type = 'left'): static
    {
        if (!empty($table) && !empty($condition)) {
            $this->join .= ' ' . strtoupper($type) . ' JOIN ' . $table . ' ON ' . $condition;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->select . ' ' . $this->from . ' ' . $this->join . ' ' . $this->where . ' ' . $this->andWhere;
    }
}