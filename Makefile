stop:
	docker compose stop
start:
	docker compose up --detach
destroy:
	docker compose down --remove-orphans --volumes
build:
	docker compose build --pull
migrate:
	cat timeweb_db.sql | docker exec -i db psql -U root -d timeweb
composer-install:
	docker compose exec app composer install
copy-env:
	cp .env.dev .env
test:
	./vendor/bin/phpunit
init: copy-env destroy build start composer-install migrate

