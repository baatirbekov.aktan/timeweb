<?php

namespace Routes;

use App\Container\Container;
use app\Traits\SingletonTrait;
use ReflectionException;

class Router
{
    use SingletonTrait;

    /**
     * @var array|mixed
     */
    private readonly array $routes;
    private Container $container;

    public function __construct()
    {
        $this->container = Container::getInstance();
        $this->routes = include(__DIR__ . '/routes.php');
    }

    /**
     * @param $request
     * @return void
     * @throws ReflectionException
     */
    public function dispatch($request): void
    {
        $method = $request['method'];
        $path = $request['path'];
        $params = $request['params'];

        foreach ($this->routes as $route) {
            list($routeMethod, $routePath, $controllerClass, $controllerMethod) = $route;

            if ($method == $routeMethod && preg_match("#^$routePath$#", $path, $matches)) {
                array_shift($matches);
                $controller = $this->container->get($controllerClass);

                if (isset($matches[0])) {
                    $controller->$controllerMethod($matches[0], $params);
                } else {
                    $controller->$controllerMethod($params);
                }
                return;
            }
        }

        header('HTTP/1.1 404 Not Found');
        die();
    }
}

