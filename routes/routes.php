<?php

use App\Controllers\CommentController;
use App\Controllers\TicketController;
use App\Controllers\UserController;

return [
    ['GET', '/api/users', UserController::class, 'index'],
    ['POST', '/api/users', UserController::class, 'store'],
    ['GET', '/api/users/(\d+)', UserController::class, 'show'],
    ['PUT', '/api/users/(\d+)', UserController::class, 'update'],
    ['DELETE', '/api/users/(\d+)', UserController::class, 'delete'],
    ['GET', '/api/tickets', TicketController::class, 'index'],
    ['POST', '/api/tickets', TicketController::class, 'store'],
    ['GET', '/api/tickets/(\d+)', TicketController::class, 'show'],
    ['PUT', '/api/tickets/(\d+)', TicketController::class, 'update'],
    ['DELETE', '/api/tickets/(\d+)', TicketController::class, 'delete'],
    ['GET', '/api/comments', CommentController::class, 'index'],
    ['POST', '/api/comments', CommentController::class, 'store'],
    ['GET', '/api/comments/(\d+)', CommentController::class, 'show'],
    ['PUT', '/api/comments/(\d+)', CommentController::class, 'update'],
    ['DELETE', '/api/comments/(\d+)', CommentController::class, 'delete'],
];