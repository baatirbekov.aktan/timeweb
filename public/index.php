<?php


require_once __DIR__ . '/../vendor/autoload.php';

$provider = new \App\Providers\AppServiceProvider();
$provider->register();

$request = [
    'method' => $_SERVER['REQUEST_METHOD'],
    'path' => parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),
    'params' => $_REQUEST
];

$router = \Routes\Router::getInstance();

try {
    $router->dispatch($request);
} catch (ReflectionException $e) {
}

