--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE timeweb;




--
-- Drop roles
--

DROP ROLE root;


--
-- Roles
--

CREATE ROLE root;
ALTER ROLE root WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:qTHSlFB7ZdtKAGqGsNwPQg==$joZjkIL8twgINIpOXIC9sQnz37JyF3DKPqVpUK1S7Wc=:SkK9BogLoGJBqFBT8AbXmTaAAvTW9ntFRJowj4ztnKo=';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO root;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: root
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: root
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: root
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO root;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: root
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- Database "timeweb" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: timeweb; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE timeweb WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE timeweb OWNER TO root;

\connect timeweb

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.comments (
    id bigint NOT NULL,
    ticket_id bigint NOT NULL,
    author_id bigint NOT NULL,
    comment text DEFAULT 'New comment'::text NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.comments OWNER TO root;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO root;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.comments_id_seq OWNED BY public.comments.id;


--
-- Name: tickets; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.tickets (
    id bigint NOT NULL,
    creator_id bigint NOT NULL,
    in_work_user_id bigint,
    title character varying(255) NOT NULL,
    status character varying(255) DEFAULT 'new'::character varying NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    CONSTRAINT tickets_status_check CHECK (((status)::text = ANY ((ARRAY['new'::character varying, 'in_work'::character varying, 'wait_answer'::character varying, 'closed'::character varying])::text[])))
);


ALTER TABLE public.tickets OWNER TO root;

--
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_id_seq OWNER TO root;

--
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.tickets_id_seq OWNED BY public.tickets.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    login character varying(255) NOT NULL,
    email character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: comments id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.comments ALTER COLUMN id SET DEFAULT nextval('public.comments_id_seq'::regclass);


--
-- Name: tickets id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tickets ALTER COLUMN id SET DEFAULT nextval('public.tickets_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.comments (id, ticket_id, author_id, comment, created_at) FROM stdin;
1	1	1	Quisquam necessitatibus repellat eaque reprehenderit maxime.	2005-03-04 03:07:10
2	1	2	Esse beatae officia consequatur qui occaecati sunt.	2022-03-09 20:52:47
3	1	3	Unde enim vel quis.	1998-12-31 16:22:31
4	1	4	Impedit sit nihil aut ipsam ut.	1971-07-10 17:34:59
5	1	5	Dolore aut velit alias.	1990-05-25 18:55:32
6	1	6	Est delectus mollitia voluptas sit.	1999-07-27 13:11:02
7	1	7	In amet vel fugit soluta quisquam facilis.	2008-11-15 09:05:27
8	1	8	Est non cum ut.	1975-06-15 02:16:35
9	1	9	Neque saepe debitis esse.	2015-06-09 19:18:05
10	1	10	In corrupti excepturi at dolorem aliquam.	1990-09-06 04:05:21
11	2	1	Qui sint eum alias nobis unde.	2010-06-04 22:40:59
12	2	2	Qui quo voluptas fugit vitae.	1970-07-19 07:57:41
13	2	3	Quasi ipsa reprehenderit qui vel iusto.	1983-10-30 00:47:11
14	2	4	Et aperiam consectetur ex.	1996-04-02 05:38:25
15	2	5	Laudantium quis dicta mollitia velit.	2006-11-28 10:59:40
16	2	6	Deserunt dolor suscipit tenetur consequatur voluptas.	2012-02-24 09:36:12
17	2	7	Quia ut qui dolorem est.	1994-06-07 23:44:03
18	2	8	Necessitatibus illo et voluptas est neque quod.	2013-05-08 14:53:31
19	2	9	Minus voluptatum nobis dolorem quis qui.	1997-06-08 09:59:37
20	2	10	Sunt voluptatibus quaerat voluptas vitae odio.	1989-07-31 01:29:32
21	3	1	Maiores laboriosam ab ut explicabo.	1970-09-23 15:32:41
22	3	2	Libero voluptatem amet pariatur.	2019-01-30 23:23:21
23	3	3	Velit reprehenderit et qui error et.	1975-05-13 00:45:10
24	3	4	Totam et nobis reiciendis expedita.	1987-05-29 19:37:28
25	3	5	Provident iste voluptas consequatur.	1986-04-12 11:24:11
26	3	6	Eum possimus quidem consequatur nihil et autem.	1978-06-18 08:59:51
27	3	7	Laborum libero vero pariatur officiis.	1978-03-02 12:00:35
28	3	8	Omnis ex eum tenetur et corrupti.	1996-07-22 06:32:33
29	3	9	Voluptas ipsum expedita ab.	2007-06-23 05:32:55
30	3	10	Nihil quam nostrum aut.	1994-02-16 23:08:35
31	4	1	Officia fugiat blanditiis et occaecati aspernatur.	2002-06-22 14:21:20
32	4	2	A voluptate architecto ut.	1987-04-05 23:43:04
33	4	3	Dolorem sit et repellendus sunt omnis rerum.	1975-07-09 12:38:00
34	4	4	Non vero neque ut sunt.	1981-09-23 13:22:22
35	4	5	Sed id autem porro ut.	2012-08-31 23:19:20
36	4	6	Dolor culpa minus illo eum voluptatem ducimus.	2011-08-05 21:21:33
37	4	7	Nesciunt officiis natus cupiditate in odio maiores temporibus.	1987-12-15 21:50:27
38	4	8	Autem eos voluptas ut officiis adipisci.	2006-06-16 15:15:31
39	4	9	Ea cumque neque quibusdam odit voluptate quia.	2002-09-16 06:11:38
40	4	10	Itaque molestiae sed ducimus.	1974-10-14 17:59:17
41	5	1	Ullam impedit cum corporis aut voluptatem.	1974-03-02 01:46:00
42	5	2	Ratione dolorum commodi est possimus omnis.	2005-01-09 00:13:41
43	5	3	Ut exercitationem est iusto quod.	1973-06-11 09:01:04
44	5	4	Velit aperiam et iste eveniet nam.	2013-05-24 00:49:58
45	5	5	Magni ipsa voluptatem recusandae saepe vitae et.	1986-08-12 09:22:24
46	5	6	Enim minima laborum recusandae corrupti modi.	2016-06-14 00:32:02
47	5	7	Suscipit perferendis et voluptatem sed.	2016-10-30 03:39:11
48	5	8	Veritatis consequatur delectus dolores similique non quo.	2006-08-17 05:25:14
49	5	9	Voluptatum modi rerum quos quis maiores.	1991-11-07 02:27:51
50	5	10	Odio amet minus ab aut consectetur fuga.	2003-07-17 14:14:40
51	6	1	Dicta ipsum eius quos quo ut qui.	1999-08-05 12:45:45
52	6	2	Laborum quis consectetur animi in nobis.	1978-02-18 11:38:08
53	6	3	Rem reiciendis dolorum est perspiciatis.	1976-08-26 12:54:25
54	6	4	Laboriosam illum dignissimos corporis.	1990-10-02 14:30:54
55	6	5	Sunt tempore ratione nisi.	1989-10-23 08:59:53
56	6	6	Repellat accusantium molestiae repudiandae rerum.	2017-09-03 17:07:30
57	6	7	Reiciendis dolorum hic pariatur.	1973-02-17 10:38:58
58	6	8	Aut tempore dolorum molestiae.	2002-02-03 03:05:40
59	6	9	Delectus sed laudantium ut ut.	2022-05-15 09:37:36
60	6	10	Inventore inventore est vitae assumenda.	1975-08-20 01:54:33
61	7	1	Ut mollitia et repellendus.	1980-06-19 13:09:37
62	7	2	Et voluptatem et aut eos eius exercitationem.	1977-11-14 17:44:26
63	7	3	Voluptatem tempore repellat sapiente.	2008-05-27 01:46:02
64	7	4	Sequi quis officia at quasi repellat.	2019-02-03 19:45:45
65	7	5	Nostrum sapiente nam blanditiis a.	1989-06-18 16:31:32
66	7	6	Et earum qui ullam incidunt iure.	1990-04-25 05:44:18
67	7	7	Nostrum ratione qui dicta sint.	2010-12-30 11:55:13
68	7	8	Eaque velit fugiat quod.	2002-02-23 21:03:13
69	7	9	Consequatur quas fugit perferendis.	1980-04-03 08:39:32
70	7	10	Et et maxime et.	1982-07-11 14:35:18
71	8	1	Sed facere possimus omnis et accusamus.	2002-07-25 05:48:48
72	8	2	Voluptas error molestiae non.	2012-04-10 10:30:14
73	8	3	Id temporibus voluptatem et voluptatem.	1981-08-31 09:56:38
74	8	4	Impedit ut sed quasi ea est.	1977-06-05 15:16:02
75	8	5	Qui eos accusamus maxime temporibus eos eaque.	2010-03-23 17:17:01
76	8	6	Adipisci dolorem non quia ut a.	1970-06-23 08:11:59
77	8	7	Magni magnam dicta et voluptatem.	1974-10-10 19:25:14
78	8	8	Commodi maiores nobis qui.	2009-10-01 12:52:28
79	8	9	Quo corrupti odit doloribus maiores magnam.	2004-10-05 17:27:52
80	8	10	Nulla quos nisi odit aut maxime tempore.	1991-05-26 04:21:36
81	9	1	Est et ea vel numquam.	1987-04-27 05:35:45
82	9	2	Repellendus illum quo omnis ipsum illum.	1973-12-06 12:48:19
83	9	3	Eum aut dolor sit deleniti labore voluptatem.	2001-01-30 07:38:37
84	9	4	Nemo dolores nesciunt neque mollitia.	1985-09-28 03:36:49
85	9	5	At ut est corrupti.	2002-07-29 03:37:37
86	9	6	Sint in itaque voluptas.	2020-07-01 07:41:31
87	9	7	Consequatur qui odio quaerat eius praesentium repellendus.	2008-03-14 23:39:02
88	9	8	In laboriosam alias iste qui.	1995-06-18 16:47:41
89	9	9	Omnis id ad ut distinctio excepturi.	1975-11-29 19:07:27
90	9	10	Suscipit quia ut sunt vel.	1980-11-28 02:55:48
91	10	1	Eveniet dolores voluptatem sit quis.	1998-04-22 21:46:49
92	10	2	Explicabo voluptas animi excepturi.	1993-10-19 16:17:12
93	10	3	Vero deserunt id quaerat nulla repellat sit.	1998-12-11 13:04:15
94	10	4	Tenetur et beatae labore quia.	2000-08-23 20:13:43
95	10	5	Nam ut voluptatem reprehenderit voluptate architecto repellat.	2008-01-21 11:38:03
96	10	6	Sint harum velit cum provident.	2020-05-11 21:21:42
97	10	7	Repudiandae rerum aspernatur ut aut voluptates.	1973-04-01 13:37:56
98	10	8	Quia rem non sint id molestiae ut.	2020-02-28 06:47:51
99	10	9	Sint facilis eum dolorum explicabo.	2022-08-08 23:26:01
100	10	10	Minima eligendi quaerat a velit illum dolor.	1996-02-02 14:45:45
101	11	1	Unde veniam officia beatae.	1996-01-17 01:23:54
102	11	2	Sunt harum et qui eaque mollitia.	2001-06-15 07:05:09
103	11	3	Necessitatibus quos et officia illo.	1998-09-13 18:26:33
104	11	4	Iste quisquam eum omnis.	1974-05-06 05:25:59
105	11	5	Ab aperiam exercitationem nisi eum distinctio.	2012-04-03 14:39:41
106	11	6	Tenetur sed et ex et.	2005-08-30 18:37:12
107	11	7	Ea quos ut sapiente molestiae.	1984-09-06 21:33:43
108	11	8	Aut dolores aut ratione consequatur.	1985-12-16 19:43:44
109	11	9	Eligendi illum eaque reprehenderit ut impedit.	1982-02-14 11:08:20
110	11	10	Expedita recusandae eaque non quo dolor.	2021-11-06 07:10:01
111	12	1	Est tenetur quasi aut eius ipsa.	2017-02-09 12:02:23
112	12	2	Dolorem dolor sit totam cum voluptas.	1980-12-18 07:44:44
113	12	3	Ipsam et accusamus est dolorum non nulla.	1984-06-16 20:44:58
114	12	4	Voluptatem facilis repellat molestias corporis.	1996-08-21 04:58:25
115	12	5	Voluptatem temporibus quasi omnis qui in esse.	1991-02-19 10:42:26
116	12	6	Eveniet est et corrupti tempora.	1982-09-18 01:42:38
117	12	7	In consequatur est aut asperiores dolore autem.	1977-12-01 09:10:39
118	12	8	Id optio veniam et inventore.	1989-01-25 23:29:46
119	12	9	Ipsum aut voluptatem voluptas sapiente.	1993-10-07 22:26:05
120	12	10	Nesciunt sit libero dolorum impedit est quas.	2001-03-28 16:46:36
121	13	1	Odio exercitationem hic omnis.	2003-10-05 06:04:55
122	13	2	Voluptas laboriosam rerum molestiae molestiae.	2003-04-19 22:51:24
123	13	3	Qui laboriosam neque reprehenderit.	1981-01-30 08:54:12
124	13	4	Eos perspiciatis ea earum.	2003-02-04 16:58:44
125	13	5	Ipsam molestiae quam corporis amet.	1992-03-24 14:18:04
126	13	6	Cupiditate est enim cumque tempore.	2010-09-29 22:00:21
127	13	7	Ut sed eligendi ab distinctio numquam occaecati.	1987-09-27 14:24:45
128	13	8	Aut dolorem nihil ipsam vitae.	2011-12-29 23:50:27
129	13	9	Accusantium rerum aliquam et corporis hic.	2003-11-28 03:42:26
130	13	10	Voluptatem natus et error.	1990-02-18 05:01:48
131	14	1	Rem veritatis nam ut amet.	1988-12-24 18:26:12
132	14	2	Cupiditate autem et asperiores aut.	2003-03-07 03:27:02
133	14	3	Dolorem aut eveniet reiciendis qui nesciunt odit.	2010-04-01 09:53:57
134	14	4	Reprehenderit animi et mollitia aut harum.	1972-11-05 05:19:51
135	14	5	Cupiditate magni nostrum quas et.	1988-08-18 07:30:09
136	14	6	Quo expedita id et non quis labore.	1976-09-02 13:36:09
137	14	7	Amet voluptates id enim praesentium eius.	1995-09-22 08:26:39
138	14	8	Excepturi occaecati temporibus dolores culpa et enim.	2007-04-21 19:45:03
139	14	9	Animi inventore odit asperiores est dolores dolorem.	1988-09-20 00:10:54
140	14	10	Omnis occaecati non dicta et.	1979-12-23 01:57:09
141	15	1	Dolorum veritatis nemo illo dolor nostrum vel.	2007-10-10 23:26:47
142	15	2	Nihil alias accusamus itaque.	1998-01-09 08:42:08
143	15	3	Recusandae necessitatibus deleniti voluptatum eius et.	2016-04-10 23:44:24
144	15	4	Nemo ex nihil consequuntur.	1978-07-09 07:20:42
145	15	5	Optio magni eos voluptate pariatur similique.	1975-08-12 05:06:30
146	15	6	At pariatur fuga eius.	2022-11-28 05:25:18
147	15	7	Illum dolore saepe itaque voluptate.	2019-12-19 01:03:54
148	15	8	Rerum distinctio similique iste a est.	1994-08-20 06:05:09
149	15	9	In magni perferendis soluta qui eaque.	1988-02-07 14:32:27
150	15	10	Magni voluptatem ut iure consectetur rerum.	1982-02-17 08:55:41
151	16	1	In quis quo aut ut placeat.	1975-05-17 04:18:19
152	16	2	Sint ab quibusdam voluptatibus distinctio quibusdam similique.	1988-08-12 00:50:51
153	16	3	Enim sint et sit.	1988-06-22 22:18:49
154	16	4	Quis alias perferendis aut et at.	2022-06-23 00:52:09
155	16	5	Error dignissimos id ullam beatae vel.	1971-06-02 19:09:58
156	16	6	Quos libero dolores fuga corrupti quia.	1993-02-05 19:56:16
157	16	7	Doloribus illum aliquam quibusdam et.	1989-12-06 22:39:52
158	16	8	Doloremque natus maiores dolores sed.	1989-02-04 02:10:08
159	16	9	Veritatis labore quis laudantium.	1996-07-11 07:28:50
160	16	10	Unde explicabo id facilis dolor earum.	2004-09-10 09:40:04
161	17	1	Voluptatum temporibus aliquid animi ullam in.	1998-05-14 07:30:12
162	17	2	Nam dolorem repellendus officia quia quo.	1976-01-15 08:12:33
163	17	3	Harum et corrupti perspiciatis odio.	2022-03-25 21:41:52
164	17	4	Quaerat sit ut natus earum qui et.	1998-12-01 15:43:08
165	17	5	Numquam voluptas minima qui ut occaecati molestias autem.	1981-07-05 19:15:19
166	17	6	Magni et earum sequi occaecati velit iusto.	2010-03-13 20:32:10
167	17	7	Voluptate excepturi aperiam vitae facere molestias minima.	1990-03-30 07:56:47
168	17	8	Aspernatur minima est accusamus nihil et.	1984-08-20 22:44:48
169	17	9	Quos velit omnis hic enim voluptatem et.	2007-01-25 12:37:01
170	17	10	Consequatur possimus nam eius.	1979-05-15 22:31:05
171	18	1	Voluptates et natus totam voluptatibus rerum.	2006-12-09 05:01:47
172	18	2	Et enim laboriosam voluptate.	1993-02-13 16:13:37
173	18	3	Autem nobis cum quos.	1999-06-19 16:10:41
174	18	4	Enim reiciendis quibusdam sint.	1989-11-20 16:22:47
175	18	5	Iusto exercitationem et iste.	1979-01-29 11:17:53
176	18	6	Laudantium ex quaerat placeat corrupti sed.	2022-05-02 06:28:25
177	18	7	Eos architecto est modi qui voluptas eos.	2014-06-03 05:08:04
178	18	8	Totam occaecati non est et.	2018-12-12 20:02:36
179	18	9	Voluptatem neque voluptas ad qui.	1989-12-18 08:04:56
180	18	10	Sapiente sint eius ipsam sed animi.	1998-07-28 06:12:14
181	19	1	Officia facilis commodi occaecati.	1972-06-09 23:03:15
182	19	2	Eum labore veniam repudiandae.	2011-01-08 11:55:15
183	19	3	Doloremque sit quod ratione vero qui quisquam.	1992-02-25 08:33:03
184	19	4	Vel esse cupiditate velit sequi dicta.	1991-07-02 19:06:42
185	19	5	Alias corporis amet cum consequatur.	2009-02-13 14:34:00
186	19	6	Ut eaque nostrum in.	1991-04-30 17:26:37
187	19	7	Consectetur culpa dicta nesciunt laboriosam voluptatem ipsum.	1975-03-25 13:31:43
188	19	8	Repellat ut tempora ipsa dolorum est.	2013-12-09 04:58:33
189	19	9	Accusamus odio neque ut explicabo.	1983-07-01 09:22:01
190	19	10	Et officia similique nesciunt.	1980-04-10 20:58:04
191	20	1	Vitae unde quia sapiente.	1988-03-27 06:37:57
192	20	2	Nulla aliquid doloremque consequatur vel et molestias.	2022-05-30 17:59:53
193	20	3	Nemo aliquam praesentium voluptas veniam tenetur sed.	1985-12-13 06:10:56
194	20	4	Dolorum modi saepe sit at excepturi.	2008-06-29 22:46:35
195	20	5	Sit provident quis corrupti tempora quam.	2005-01-16 05:45:28
196	20	6	Nihil eos ipsum aut quidem.	2016-10-24 20:33:48
197	20	7	In deleniti unde est excepturi.	1976-01-29 15:12:39
198	20	8	Sed aut officiis architecto sunt explicabo ipsum.	1998-10-16 10:33:51
199	20	9	Esse dolor voluptatibus aut maxime est eius.	1973-07-26 15:50:14
200	20	10	Ullam eum atque aut.	2020-12-15 21:48:24
\.


--
-- Data for Name: tickets; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.tickets (id, creator_id, in_work_user_id, title, status, created_at) FROM stdin;
1	1	9	Corene Kunde	new	1989-08-29 13:04:03
2	1	9	Marco Williamson	new	2005-11-19 08:35:19
3	2	5	Patricia Jones	new	1995-07-19 22:08:37
4	2	5	Annabell Waelchi	new	2006-04-25 02:30:25
5	3	5	Jo Fritsch MD	new	1985-02-22 04:23:40
6	3	5	Allan Stamm	new	1970-05-10 16:39:08
7	4	9	Dr. Norwood Rohan III	new	1982-01-12 10:03:51
8	4	9	Mrs. Megane Boyer II	new	2022-05-11 19:58:42
9	5	7	Antonette Walter Sr.	new	1982-12-10 05:37:43
10	5	7	Cristal Boehm	new	1970-05-24 21:56:48
11	6	8	Norma O'Hara	new	2000-03-17 07:22:31
12	6	8	Jayda Hodkiewicz	new	1982-12-13 06:21:33
13	7	1	Reilly Weimann	new	1994-10-06 12:54:09
14	7	1	Prof. Justus Wisoky	new	2000-04-22 03:18:18
15	8	7	Dr. Roy Ward	new	2006-04-20 21:14:56
16	8	7	Hardy Kessler Jr.	new	2009-09-06 20:30:49
17	9	1	Boyd Ritchie	new	2003-10-04 22:36:10
18	9	1	Dr. Kennedy Carter DDS	new	2012-03-31 05:25:17
19	10	1	Miss Lia Gislason V	new	1991-12-01 22:47:57
20	10	1	Prof. Keeley Rempel	new	2013-02-01 03:04:58
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users (id, login, email) FROM stdin;
1	Kellie Reichert	sstrosin@example.org
2	Bert Howe	jailyn.mayer@example.com
3	Miss Rosalinda Wiza V	adriel44@example.com
4	Emmanuel Padberg DDS	serenity.jaskolski@example.net
5	Marc Eichmann	lupe.little@example.com
6	Dr. Eleanore Kohler V	russel.maureen@example.org
7	Nola Kilback V	reyes10@example.net
8	Prof. Brenden Blanda	braun.annabelle@example.org
9	Sydney Gleichner	qromaguera@example.org
10	Nels Veum	koconner@example.com
\.


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.comments_id_seq', 200, true);


--
-- Name: tickets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.tickets_id_seq', 20, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_id_seq', 10, true);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: tickets tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_login_unique; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_login_unique UNIQUE (login);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: comments comments_author_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_author_id_foreign FOREIGN KEY (author_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comments comments_ticket_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_ticket_id_foreign FOREIGN KEY (ticket_id) REFERENCES public.tickets(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tickets tickets_creator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_creator_id_foreign FOREIGN KEY (creator_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tickets tickets_in_work_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_in_work_user_id_foreign FOREIGN KEY (in_work_user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

