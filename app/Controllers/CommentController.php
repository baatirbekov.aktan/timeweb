<?php

namespace App\Controllers;

use App\Enums\Status;
use App\Http\JsonResponse;
use App\Repositories\CommentRepository;
use App\Repositories\TicketRepository;
use App\Validation\Validator;
use App\Validation\ValidatorInterface;
use Exception;

class CommentController
{
    /**
     * @param CommentRepository $commentRepository
     * @param TicketRepository $ticketRepository
     * @param Validator $validator
     */
    public function __construct(
        private readonly CommentRepository  $commentRepository,
        private readonly TicketRepository   $ticketRepository,
        private readonly ValidatorInterface $validator
    )
    {
    }

    /**
     * @param $params
     * @return void
     */
    public function index($params): void
    {
        $comments = $this->commentRepository->getCommentsWithRelations($params);
        $response = new JsonResponse(['data' => $comments], 200);
        $response->send();
    }

    /**
     * @param $id
     * @return void
     */
    public function show($id): void
    {
        $comment = $this->commentRepository->getById($id);
        $response = new JsonResponse(['data' => $comment], 200);
        $response->send();
    }

    /**
     * @param $request
     * @return void
     * @throws Exception
     */
    public function store($request): void
    {
        $rules = [
            'author_id' => ['required', 'numeric'],
            'comment' => ['required'],
            'ticket_id' => ['required', 'numeric'],
        ];

        $this->validator->validate($request, $rules);

        $date = new \DateTime();

        $comment = $this->commentRepository->save([
            'comment' => $request['comment'],
            'author_id' => $request['author_id'],
            'ticket_id' => $request['ticket_id'],
            'created_at' => $date->format('Y-m-d H:i:s'),
        ]);

        $this->ticketRepository->update($request['ticket_id'], [
            'status' => Status::WaitAnswer->value,
            'in_work_user_id' => null
        ]);

        $response = new JsonResponse(['data' => $comment], 201);
        $response->send();
    }

    /**
     * @param $id
     * @param $request
     * @return void
     */
    public function update($id, $request): void
    {
        $user = $this->commentRepository->update($id, $request);
        $response = new JsonResponse(['data' => $user], 200);
        $response->send();
    }

    /**
     * @param $id
     * @return void
     */
    public function delete($id): void
    {
        $this->commentRepository->delete($id);
        $response = new JsonResponse([], 204);
        $response->send();
    }
}