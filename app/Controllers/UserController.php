<?php

namespace App\Controllers;

use App\Http\JsonResponse;
use App\Repositories\UserRepository;

class UserController
{
    /**
     * @param UserRepository $userRepository
     */
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    /**
     * @return void
     */
    public function index(): void
    {
        $users = $this->userRepository->getAll();
        $response = new JsonResponse(['data' => $users], 200);
        $response->send();
    }

    /**
     * @param $id
     * @return void
     */
    public function show($id): void
    {
        $user = $this->userRepository->getById($id);
        $response = new JsonResponse(['data' => $user], 200);
        $response->send();
    }

    /**
     * @param $request
     * @return void
     */
    public function store($request): void
    {
        $user = $this->userRepository->save($request);
        $response = new JsonResponse(['data' => $user], 200);
        $response->send();
    }

    /**
     * @param $id
     * @param $request
     * @return void
     */
    public function update($id, $request): void
    {
        $user = $this->userRepository->update($id, $request);
        $response = new JsonResponse(['data' => $user], 200);
        $response->send();
    }

    /**
     * @param $id
     * @return void
     */
    public function delete($id): void
    {
        $this->userRepository->delete($id);
        $response = new JsonResponse([], 204);
        $response->send();
    }
}