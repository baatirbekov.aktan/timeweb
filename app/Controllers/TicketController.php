<?php

namespace App\Controllers;

use App\Enums\Status;
use App\Http\JsonResponse;
use App\Repositories\CommentRepository;
use App\Repositories\TicketRepository;
use App\Validation\Validator;
use App\Validation\ValidatorInterface;
use Exception;

class TicketController
{
    /**
     * @param TicketRepository $ticketRepository
     * @param CommentRepository $commentRepository
     * @param Validator $validator
     */
    public function __construct(
        private readonly TicketRepository   $ticketRepository,
        private readonly CommentRepository  $commentRepository,
        private readonly ValidatorInterface $validator
    )
    {
    }

    /**
     * @param $request
     * @return void
     */
    public function index($request): void
    {
        $tickets = $this->ticketRepository->getTicketsWithRelations($request);
        $response = new JsonResponse(['data' => $tickets], 200);
        $response->send();
    }

    /**
     * @param $id
     * @return void
     */
    public function show($id): void
    {
        $ticket = $this->ticketRepository->getById($id);
        $response = new JsonResponse(['data' => $ticket], 200);
        $response->send();
    }

    /**
     * @param $request
     * @return void
     * @throws Exception
     */
    public function store($request): void
    {
        $rules = [
            'creator_id' => ['required', 'numeric'],
            'comment' => ['required'],
            'title' => ['required'],
        ];

        $this->validator->validate($request, $rules);

        $date = new \DateTime();

        $ticket = $this->ticketRepository->save([
            'status' => Status::New->value,
            'title' => $request['title'],
            'creator_id' => $request['creator_id'],
            'created_at' => $date->format('Y-m-d H:i:s'),
        ]);

        $this->commentRepository->save([
            'author_id' => $request['creator_id'],
            'ticket_id' => $ticket->getId(),
            'comment' => $request['comment'],
            'created_at' => $date->format('Y-m-d H:i:s'),
        ]);

        $response = new JsonResponse(['data' => $ticket], 201);
        $response->send();
    }

    /**
     * @param $id
     * @param $request
     * @return void
     * @throws Exception
     */
    public function update($id, $request): void
    {
        $rules = [
            'in_work_user_id' => ['required', 'numeric'],
            'status' => ['required', ['in_array', array_column(Status::cases(), 'value')]]
        ];

        $this->validator->validate($request, $rules);

        $ticket = $this->ticketRepository->update($id, [
            'in_work_user_id' => $request['in_work_user_id'],
            'status' => $request['status'],
        ]);

        $response = new JsonResponse(['data' => $ticket], 200);
        $response->send();
    }

    /**
     * @param $id
     * @return void
     */
    public function delete($id): void
    {
        $this->ticketRepository->delete($id);
        $response = new JsonResponse([], 204);
        $response->send();
    }
}