<?php

namespace App\Http;

class JsonResponse
{
    /**
     * @param array $data
     * @param int $statusCode
     */
    public function __construct(private readonly array $data, private readonly int $statusCode = 200)
    {
    }

    /**
     * @return void
     */
    public function send(): void
    {
        header('Content-Type: application/json');
        http_response_code($this->statusCode);
        echo json_encode($this->data, JSON_PRETTY_PRINT);
    }
}