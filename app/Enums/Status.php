<?php

namespace App\Enums;

enum Status: string
{
    case InWork = 'in_work';

    case New = 'new';

    case WaitAnswer = 'wait_answer';

    case Closed = 'closed';
}
