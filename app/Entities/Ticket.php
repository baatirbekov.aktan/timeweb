<?php

namespace App\Entities;

use JsonSerializable;

class Ticket implements JsonSerializable
{
    /**
     * @param int $id
     * @param int $creator_id
     * @param int|null $in_work_user_id
     * @param string $status
     * @param string $title
     * @param string $created_at
     */
    public function __construct(
        private int    $id,
        private int    $creator_id,
        private ?int   $in_work_user_id,
        private string $status,
        private string $title,
        private string $created_at
    ) {}

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreatorId(): int
    {
        return $this->creator_id;
    }

    /**
     * @param int $creatorId
     */
    public function setCreatorId(int $creatorId): void
    {
        $this->creator_id = $creatorId;
    }

    /**
     * @return int|null
     */
    public function getInWorkUserId(): ?int
    {
        return $this->in_work_user_id;
    }

    /**
     * @param int|null $inWorkUserId
     */
    public function setInWorkUserId(?int $inWorkUserId): void
    {
        $this->in_work_user_id = $inWorkUserId;
    }

    /**
     * @return int
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'creator_id' => $this->getCreatorId(),
            'in_work_user_id' => $this->getInWorkUserId(),
            'title' => $this->getTitle(),
            'status' => $this->getStatus(),
            'created_at' => $this->getCreatedAt()
        ];
    }
}