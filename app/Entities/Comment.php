<?php

namespace App\Entities;

use JsonSerializable;

class Comment implements JsonSerializable
{
    /**
     * @param int $id
     * @param int $ticket_id
     * @param int $author_id
     * @param string $comment
     * @param string $created_at
     */
    public function __construct(
        private int    $id,
        private int    $ticket_id,
        private int    $author_id,
        private string $comment,
        private string $created_at
    ) {}

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTicketId(): int
    {
        return $this->ticket_id;
    }

    /**
     * @param int $ticketId
     */
    public function setTicketId(int $ticketId): void
    {
        $this->ticket_id = $ticketId;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->author_id;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId(int $authorId): void
    {
        $this->author_id = $authorId;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'author_id' => $this->getAuthorId(),
            'ticket_id' => $this->getTicketId(),
            'comment' => $this->getComment(),
            'created_at' => $this->getCreatedAt()
        ];
    }
}