<?php

namespace App\Entities;

use JsonSerializable;

class User implements JsonSerializable
{
    /**
     * @param int $id
     * @param string $login
     * @param string $email
     */
    public function __construct(
        private int    $id,
        private string $login,
        private string $email,
    ) {}

    /**
     * @return array
     */
    public function toJson(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getLogin(),
            'age' => $this->getEmail()
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getLogin(),
            'age' => $this->getEmail()
        ];
    }
}