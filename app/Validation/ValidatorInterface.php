<?php

namespace App\Validation;

interface ValidatorInterface
{
    /**
     * @param $inputData
     * @param $rules
     * @return array
     */
    public function validate($inputData, $rules): array;
}