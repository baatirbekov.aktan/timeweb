<?php

namespace App\Validation;

use App\Validation\Types\EmailValidationType;
use App\Validation\Types\InArrayValidationType;
use App\Validation\Types\NumericValidationType;
use App\Validation\Types\RequiredValidationType;
use App\Validation\Types\ValidationTypeInterface;
use Exception;

class ValidatorFactory
{
    /**
     * @param $type
     * @param $options
     * @return ValidationTypeInterface
     * @throws Exception
     */
    public static function makeValidator($type, $options = null): ValidationTypeInterface
    {
        return match ($type) {
            'required' => new RequiredValidationType(),
            'numeric' => new NumericValidationType(),
            'email' => new EmailValidationType(),
            'in_array' => new InArrayValidationType($options),
            default => throw new Exception('Invalid validator type.'),
        };
    }
}