<?php

namespace App\Validation;

use App\Http\JsonResponse;
use Exception;

class Validator implements ValidatorInterface
{
    /**
     * @throws Exception
     */
    public function validate($inputData, $rules): array
    {
        $errors = [];
        foreach ($rules as $field => $fieldRules) {
            $fieldValue = $inputData[$field] ?? null;
            foreach ($fieldRules as $rule) {
                if (is_array($rule)) {
                    $validator = ValidatorFactory::makeValidator($rule[0], $rule[1]);
                } else {
                    $validator = ValidatorFactory::makeValidator($rule);
                }
                if ($validator->validate($fieldValue, $field)) {
                    $errors[] = $validator->validate($fieldValue, $field);
                }
            }
        }

        if (!empty($errors)) {
            $response = new JsonResponse([
                'errors' => $errors
            ], 422);
            $response->send();
            die();
        }

        return $errors;
    }
}