<?php

namespace App\Validation\Types;

class InArrayValidationType implements ValidationTypeInterface
{
    private array $options;

    /**
     * @param $options
     */
    public function __construct($options) {
        $this->options = $options;
    }

    /**
     * @param $value
     * @param $field
     * @return array|null
     */
    public function validate($value, $field): ?array
    {
        if (!in_array($value, $this->options)) {
            return ['field' => $field,'message' => "$field must be one of " . implode(', ', $this->options)];
        }
        return null;
    }
}