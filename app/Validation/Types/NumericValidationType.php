<?php

namespace App\Validation\Types;

class NumericValidationType implements ValidationTypeInterface
{
    /**
     * @param $value
     * @param $field
     * @return array|null
     */
    public function validate($value, $field): ?array
    {
        if (!is_numeric($value)) {
            return ['field' => $field,'message' => "$field must be numeric."];
        }
        return null;
    }
}