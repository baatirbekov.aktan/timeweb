<?php

namespace App\Validation\Types;

interface ValidationTypeInterface
{
    /**
     * @param $value
     * @param $field
     * @return mixed
     */
    public function validate($value, $field): mixed;
}