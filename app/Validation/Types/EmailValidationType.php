<?php

namespace App\Validation\Types;

class EmailValidationType implements ValidationTypeInterface
{
    /**
     * @param $value
     * @param $field
     * @return array|null
     */
    public function validate($value, $field): ?array
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return ['field' => $field,'message' => "$field must be a valid email address."];
        }
        return null;
    }
}