<?php

namespace App\Validation\Types;

class RequiredValidationType implements ValidationTypeInterface
{
    /**
     * @param $value
     * @param $field
     * @return array|null
     */
    public function validate($value, $field): ?array
    {
        if ($value == null || empty(trim($value))) {
            return ['field' => $field,'message' => "$field  is required."];
        }
        return null;
    }
}