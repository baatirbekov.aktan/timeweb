<?php

namespace App\Repositories;

use App\Entities\Ticket;
use PDO;

class TicketRepository extends BaseRepository
{
    /**
     * @var string
     */
    public string $table = 'tickets';

    /**
     * @var string
     */
    public string $entity = Ticket::class;

    /**
     * @param $params
     * @return bool|array
     */
    public function getTicketsWithRelations($params): bool|array
    {
        $this->queryBuilder->select([
            't.id',
            't.title',
            't.status',
            "JSON_BUILD_OBJECT('id', cu.id, 'login', cu.login, 'email', cu.email) AS creator",
            "JSON_BUILD_OBJECT('id', iwu.id, 'login', iwu.login, 'email', iwu.email) AS in_work_user",
            't.created_at'])
            ->from('tickets as t')
            ->join('users AS cu', 't.creator_id = cu.id')
            ->join('users AS iwu', 't.in_work_user_id = iwu.id');

        if (isset($params['status'])) {
            $this->queryBuilder->where("t.status = '{$params['status']}'");
        }

        if (isset($params['creator_id'])) {
            $this->queryBuilder->where("t.creator_id = '{$params['creator_id']}'");
        }

        if (isset($params['in_work_user_id'])) {
            $this->queryBuilder->where("t.in_work_user_id = '{$params['in_work_user_id']}'");
        }

        $query = $this->queryBuilder->getQuery();

        $stmt = $this->conn->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results as &$result) {
            $result['creator'] = json_decode($result['creator'], true);
            $result['in_work_user'] = json_decode($result['in_work_user'], true);
        }

        return $results;
    }
}