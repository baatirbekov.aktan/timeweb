<?php

namespace App\Repositories;

use App\Entities\Comment;
use PDO;

class CommentRepository extends BaseRepository
{
    /**
     * @var string
     */
    public string $table = 'comments';

    /**
     * @var string
     */
    public string $entity = Comment::class;

    /**
     * @param $params
     * @return bool|array
     */
    public function getCommentsWithRelations($params): bool|array
    {
        $this->queryBuilder->select([
            'c.id',
            'c.comment',
            "JSON_BUILD_OBJECT('id', u.id, 'login', u.login, 'email', u.email) AS author",
            "JSON_BUILD_OBJECT('id', t.id, 'creator_id', t.creator_id, 'in_work_user_id', t.in_work_user_id, 'status', t.status, 'title', t.title, 'created_at', t.created_at) AS ticket",
            'c.created_at'])
            ->from('comments as c')
            ->join('users as u', 'c.author_id = u.id')
            ->join('tickets as t', 'c.ticket_id = t.id');

        if (isset($params['author_id'])) {
            $this->queryBuilder->where("c.author_id = '{$params['author_id']}'");
        }

        if (isset($params['ticket_id'])) {
            $this->queryBuilder->where("c.ticket_id = '{$params['ticket_id']}'");
        }

        $query = $this->queryBuilder->getQuery();

        $stmt = $this->conn->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results as &$result) {
            $result['author'] = json_decode($result['author'], true);
            $result['ticket'] = json_decode($result['ticket'], true);
        }

        return $results;
    }
}