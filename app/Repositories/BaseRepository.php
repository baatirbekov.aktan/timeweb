<?php

namespace App\Repositories;

use Database\Database;
use Database\QueryBuilder;
use PDO;

abstract class BaseRepository
{
    /**
     * @var string
     */
    public string $table;

    /**
     * @var string
     */
    public string $entity;

    /**
     * @var PDO
     */
    protected PDO $conn;

    /**
     * @var QueryBuilder
     */
    protected QueryBuilder $queryBuilder;

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->conn = Database::getInstance()->getConnection();
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    public function getById(int $id): mixed
    {
        $query = $this->queryBuilder
            ->select(['*'])
            ->from($this->table)
            ->where("id = {$id}")
            ->getQuery();

        $stmt = $this->conn->query($query);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$result) {
            return null;
        }

        return new $this->entity(...$result);
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $query = $this->queryBuilder
            ->select(['*'])
            ->from($this->table)
            ->getQuery();

        $stmt = $this->conn->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $collection = [];

        foreach ($results as $row) {
            $collection[] = new $this->entity(...$row);
        }

        return $collection;
    }

    /**
     * @param $params
     * @return mixed|null
     */
    public function save($params): mixed
    {
        $keys = array_keys($params);
        $columns = implode(',', $keys);
        $values = implode(',', array_fill(0, count($keys), '?'));

        $query = "INSERT INTO {$this->table} ({$columns}) VALUES ({$values})";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(array_values($params));

        return $this->getById($this->conn->lastInsertId());
    }

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params): mixed
    {
        $columns = array_keys($params);
        $values = array_values($params);
        $placeholders = implode('= ?,', $columns) . '=?';
        $query = "UPDATE {$this->table} SET {$placeholders} WHERE id = ?";
        $values[] = $id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute($values);

        return $this->getById($id);
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $query = "DELETE FROM {$this->table} WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->execute([$id]);
    }
}