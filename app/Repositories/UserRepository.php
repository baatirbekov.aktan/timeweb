<?php

namespace App\Repositories;

use App\Entities\User;

class UserRepository extends BaseRepository
{
    /**
     * @var string
     */
    public string $table = 'users';

    /**
     * @var string
     */
    public string $entity = User::class;
}