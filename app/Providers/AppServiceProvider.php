<?php

namespace App\Providers;

use App\Container\Container;

class AppServiceProvider
{
    /**
     * @var Container
     */
    private Container $container;

    public function __construct()
    {
        $this->container = Container::getInstance();
    }

    /**
     * @return void
     */
    public function register(): void
    {
        $this->container->bind(\App\Validation\ValidatorInterface::class, \App\Validation\Validator::class);
    }
}