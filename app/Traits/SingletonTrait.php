<?php

namespace App\Traits;

use Exception;

trait SingletonTrait
{
    private static mixed $instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    /**
     * @throws Exception
     */
    public function __unserialize(array $data)
    {
        throw new Exception("Cannot unserialize a singleton.");
    }

    /**
     * @return mixed
     */
    public static function getInstance(): mixed
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}