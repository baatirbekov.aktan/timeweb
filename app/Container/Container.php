<?php

namespace App\Container;

use App\Traits\SingletonTrait;
use ReflectionClass;
use ReflectionException;

class Container
{
    use SingletonTrait;

    /**
     * @var array
     */
    public array $services = [];

    /**
     * @param string $name
     * @param mixed $service
     * @return void
     */
    public function register(string $name, mixed $service): void
    {
        $this->services[$name] = $service;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        return array_key_exists($name, $this->services);
    }

    /**
     * @param string $name
     * @return mixed
     * @throws ReflectionException
     */
    public function get(string $name): mixed
    {
        if (interface_exists($name)) {
            $class = $this->services[$name];
            return $this->buildClass($class);
        } else if (class_exists($name)) {
            return $this->buildClass($name);
        }

        return $this->services[$name];
    }

    /**
     * @throws ReflectionException
     */
    public function buildClass($name): mixed
    {
        $reflectionClass = new ReflectionClass($name);
        $constructor = $reflectionClass->getConstructor();
        $parameters = $constructor ? $constructor->getParameters() : [];
        return new $name(...$this->getDependencies($parameters));
    }

    /**
     * @param array $parameters
     * @return array
     * @throws ReflectionException
     */
    public function getDependencies(array $parameters): array
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            $position = $parameter->getPosition();
            $name = $parameter->getType() && !$parameter->getType()->isBuiltin()
                ? $parameter->getType()->getName()
                : null;
            $dependencies[$position] = $this->get($name);
        }
        return $dependencies;
    }

    /**
     * @param string $interface
     * @param string $class
     * @return void
     */
    public function bind(string $interface, string $class): void
    {
        $this->register($interface, $class);
    }
}