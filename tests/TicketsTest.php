<?php

namespace tests;

use App\Enums\Status;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class TicketsTest extends TestCase
{
    protected function setUp(): void
    {
        $this->client = new Client();
    }

    public function testCreateTicket()
    {
        $options = [
            'multipart' => [
                [
                    'name' => 'comment',
                    'contents' => 'test comment'
                ],
                [
                    'name' => 'creator_id',
                    'contents' => '5'
                ],
                [
                    'name' => 'ticket_id',
                    'contents' => '5'
                ],
                [
                    'name' => 'title',
                    'contents' => 'test title'
                ],
                [
                    'name' => 'author_id',
                    'contents' => '1'
                ]
            ]];
        $request = new Request('POST', 'http://localhost:8000/api/tickets');
        $response = $this->client->sendAsync($request, $options)->wait();

        $this->assertEquals(201, $response->getStatusCode());

        $ticket = json_decode($response->getBody(), true);

        foreach ($ticket as $field) {
            $this->assertArrayHasKey('id', $field);
            $this->assertArrayHasKey('title', $field);
            $this->assertArrayHasKey('creator_id', $field);
            $this->assertArrayHasKey('in_work_user_id', $field);
            $this->assertArrayHasKey('status', $field);
            $this->assertArrayHasKey('created_at', $field);
        }
    }

    public function testGetTickets()
    {
        $request = new Request('GET', 'http://localhost:8000/api/tickets');
        $response = $this->client->sendAsync($request)->wait();

        $this->assertEquals(200, $response->getStatusCode());

        $tickets = json_decode($response->getBody(), true);

        foreach ($tickets['data'] as $ticket) {
            $this->assertArrayHasKey('id', $ticket);
            $this->assertArrayHasKey('title', $ticket);
            $this->assertArrayHasKey('creator', $ticket);
            $this->assertArrayHasKey('in_work_user', $ticket);
            $this->assertArrayHasKey('status', $ticket);
            $this->assertArrayHasKey('created_at', $ticket);
        }
    }

    public function testGetTicketsByInWorkUserId()
    {
        $inoWorkUserId = 5;

        $request = new Request('GET', 'http://localhost:8000/api/tickets?in_work_user_id=' . $inoWorkUserId);
        $response = $this->client->sendAsync($request)->wait();

        $this->assertEquals(200, $response->getStatusCode());

        $tickets = json_decode($response->getBody(), true);

        foreach ($tickets['data'] as $ticket) {
            $this->assertEquals($inoWorkUserId, $ticket['in_work_user']['id']);
            $this->assertArrayHasKey('id', $ticket);
            $this->assertArrayHasKey('title', $ticket);
            $this->assertArrayHasKey('creator', $ticket);
            $this->assertArrayHasKey('in_work_user', $ticket);
            $this->assertArrayHasKey('status', $ticket);
            $this->assertArrayHasKey('created_at', $ticket);
        }
    }

    public function testGetTicketsByStatus()
    {
        $status = 'new';
        $request = new Request('GET', 'http://localhost:8000/api/tickets?status=' . $status);
        $response = $this->client->sendAsync($request)->wait();

        $this->assertEquals(200, $response->getStatusCode());

        $tickets = json_decode($response->getBody(), true);

        foreach ($tickets['data'] as $ticket) {
            $this->assertEquals($status, $ticket['status']);
            $this->assertArrayHasKey('id', $ticket);
            $this->assertArrayHasKey('title', $ticket);
            $this->assertArrayHasKey('creator', $ticket);
            $this->assertArrayHasKey('in_work_user', $ticket);
            $this->assertArrayHasKey('status', $ticket);
            $this->assertArrayHasKey('created_at', $ticket);
        }
    }

    public function testGetTicketsByStatusAndByInWorkUserId()
    {
        $status = Status::New->value;
        $inoWorkUserId = 5;
        $request = new Request('GET', "http://localhost:8000/api/tickets?in_work_user_id=$inoWorkUserId&status=$status");
        $response = $this->client->sendAsync($request)->wait();

        $this->assertEquals(200, $response->getStatusCode());

        $tickets = json_decode($response->getBody(), true);

        foreach ($tickets['data'] as $ticket) {
            $this->assertEquals($status, $ticket['status']);
            $this->assertEquals($inoWorkUserId, $ticket['in_work_user']['id']);
            $this->assertArrayHasKey('id', $ticket);
            $this->assertArrayHasKey('title', $ticket);
            $this->assertArrayHasKey('creator', $ticket);
            $this->assertArrayHasKey('in_work_user', $ticket);
            $this->assertArrayHasKey('status', $ticket);
            $this->assertArrayHasKey('created_at', $ticket);
        }
    }

    public function testUpdateTicket()
    {
        $status = Status::InWork->value;
        $inoWorkUserId = 5;
        $request = new Request('PUT', "http://localhost:8000/api/tickets/5?in_work_user_id=$inoWorkUserId&status=$status");
        $response = $this->client->sendAsync($request)->wait();

        $this->assertEquals(200, $response->getStatusCode());

        $ticket = json_decode($response->getBody(), true);

        foreach ($ticket as $field) {
            $this->assertEquals($status, $field['status']);
            $this->assertEquals($inoWorkUserId, $field['in_work_user_id']);
            $this->assertArrayHasKey('id', $field);
            $this->assertArrayHasKey('title', $field);
            $this->assertArrayHasKey('creator_id', $field);
            $this->assertArrayHasKey('in_work_user_id', $field);
            $this->assertArrayHasKey('status', $field);
            $this->assertArrayHasKey('created_at', $field);
        }
    }
}
