<?php

namespace tests;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class CommentsTest extends TestCase
{
    protected function setUp(): void
    {
        $this->client = new Client();
    }

    public function testCreateTicket()
    {
        $options = [
            'multipart' => [
                [
                    'name' => 'comment',
                    'contents' => 'test comment'
                ],
                [
                    'name' => 'ticket_id',
                    'contents' => '5'
                ],
                [
                    'name' => 'author_id',
                    'contents' => '1'
                ]
            ]];
        $request = new Request('POST', 'http://localhost:8000/api/comments');
        $response = $this->client->sendAsync($request, $options)->wait();

        $comment = json_decode($response->getBody(), true);

        foreach ($comment as $field) {
            $this->assertArrayHasKey('id', $field);
            $this->assertArrayHasKey('comment', $field);
            $this->assertArrayHasKey('author_id', $field);
            $this->assertArrayHasKey('ticket_id', $field);
            $this->assertArrayHasKey('created_at', $field);
        }

        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testGetTickets()
    {
        $request = new Request('GET', 'http://localhost:8000/api/comments');
        $response = $this->client->sendAsync($request)->wait();

        $comments = json_decode($response->getBody(), true);

        foreach ($comments['data'] as $comment) {
            $this->assertArrayHasKey('id', $comment);
            $this->assertArrayHasKey('comment', $comment);
            $this->assertArrayHasKey('author', $comment);
            $this->assertArrayHasKey('ticket', $comment);
            $this->assertArrayHasKey('created_at', $comment);
        }
    }
}
